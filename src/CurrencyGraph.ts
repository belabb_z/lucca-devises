import { Graph } from './Graph';
import { ExchangeRate } from './ExchangeRate';
import { PathFinder } from './PathFinder';

export class CurrencyGraph extends Graph<string> {
  constructor() {
    super();
  }

  addEdge(from: string, to: string, amount: number): void {
    const vertexFrom = this.adjList.get(from);
    const vertexTo = this.adjList.get(to);

    if (!vertexFrom || !vertexTo) {
      throw new Error(`One or both of the mentioned vertices do not exist. [${from} or ${to}]`);
    }
    vertexFrom.push(new ExchangeRate(from, to, amount));
    vertexTo.push(new ExchangeRate(to, from, 1 / amount));
  }

  shortestPath(from: string, to: string): string[] {
    const solver: PathFinder = new PathFinder(this);

    return solver.solve(from, to);
  }
}
