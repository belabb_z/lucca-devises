import fs from 'fs';
import readline from 'readline';
import { ExchangeRate } from './ExchangeRate';
import { Graph } from './Graph';
import { CurrencyGraph } from './CurrencyGraph';

// Parses and holds the input file's data.
export class FileData {
  constructor(filename?: string) {
    if (filename) {
      this.load(filename);
    }
  }

  load(filename: string) {
    try {
      this.stream = fs.createReadStream(filename);
      this.rl = readline.createInterface({
        input: this.stream,
        crlfDelay: Infinity,
      });
    }
    catch (e) { throw new Error(`Could not read file: ${e.message}`); }
  }

  async parse() {
    let i: number = 0;

    if (!this.stream) {
      throw new Error('No file loaded.');
    }
    for await (const line of this.rl) {
      const parsedLine = line.split(';');

      if (i === 0) {
        this.parseQuery(parsedLine);
      } else if (i === 1) {
        this.parseRowCount(parsedLine);
      } else if (i < this.currencyParseLimit + 2) {
        this.parseCurrency(parsedLine, i);
      }
      i += 1;
    }
  }

  private parseQuery(line: string[]) {
    if (3 !== line.length) {
      throw new Error('Wrong format on line 1 of the input file. Must be like EUR;550;JPY');
    }
    this.from = line[0];
    this.amount = parseInt(line[1], 10);
    this.to = line[2];
  }

  private parseRowCount(line: string[]) {
    this.currencyParseLimit = parseInt(line[0], 10);
  }

  private parseCurrency(line: string[], i: number) {
    if (3 !== line.length) {
      throw new Error(`Wrong format on line ${i} of the input file. Must be like AUD;CHF;0.9661`);
    }
    if (!this.currenciesList.includes(line[0])) {
      this.currenciesGraph.addVertex(line[0]);
      this.currenciesList.push(line[0]);
    }
    if (!this.currenciesList.includes(line[1])) {
      this.currenciesGraph.addVertex(line[1]);
      this.currenciesList.push(line[1]);
    }
    this.currenciesGraph.addEdge(line[0], line[1], parseFloat(line[2]));
  }

  private rl!: readline.Interface;
  private stream!: fs.ReadStream;
  from: string = '';
  to: string = '';
  amount: number = 0;
  currencyParseLimit: number = 0;

  // Each currency is a vertex of the graph.
  // Each edge is the exchange rate from one currency to the other.
  currenciesGraph: CurrencyGraph = new CurrencyGraph();
  // List of available currencies
  currenciesList: string[] = [];
}
