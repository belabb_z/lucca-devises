import { FileData } from './FileData';
import { CurrencyConverter } from './CurrencyConverter';

main();

async function main() {
  try {
    const filename: string = process.argv[2];
    let path: string[] = [];
    let result: number = 0;

    if (!filename) {
      console.error(`Usage: ${process.execPath} <filename>`);
      process.exit(1);
    }

    const file: FileData = new FileData(filename);

    await file.parse();
    path = file.currenciesGraph.shortestPath(file.from, file.to);
    result = CurrencyConverter.compute(path, file.amount, file.currenciesGraph);
    console.log(result.toFixed(0));
  } catch (error) {
    console.error(`An error occured: ${error.message}`);
    process.exit(1);
  }
}
