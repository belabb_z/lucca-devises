import { CurrencyGraph } from './CurrencyGraph';
import { ExchangeRate } from './ExchangeRate';

export class PathFinder {
  constructor(private readonly graph: CurrencyGraph) {
  }

  solve(source: string, target: string): string[] {
    this.queue.push(source);
    this.visited.push(source);

    while (0 !== this.queue.length) {
      const current: string | undefined = this.queue.shift();
      let neighbors: ExchangeRate[] | undefined;

      if (!current) {
        throw new Error('Unexpected error.');
      }

      neighbors = this.graph.getNeighbors(current);

      if (!neighbors) {
        throw new Error('Unexpected error.');
      }

      // remove visited neighbors
      neighbors = neighbors.filter((val) => {
        for (const vertex of this.visited) {
          if (vertex === val.to) {
            return false;
          }
        }
        return true;
      });
      for (const neighbor of neighbors) {
        this.visited.push(neighbor.to);

        if (neighbor.to === target) {
          let pathCurrent: string = current;

          this.path.push(target);
          while (pathCurrent !== source) {
            this.path.push(pathCurrent);
            pathCurrent = this.predecessor[pathCurrent];
          }
          this.path.push(pathCurrent);
          this.path.reverse();
          return this.path;
        }

        this.queue.push(neighbor.to);
        this.predecessor[neighbor.to] = current;
      }
    }

    throw new Error('Impossible conversion');
  }

  private queue: string[] = [];
  private visited: string[] = [];
  private path: string[] = [];
  private predecessor: any = {};
}
