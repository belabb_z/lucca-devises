import { ExchangeRate } from './ExchangeRate';

// Graph implementation
export abstract class Graph<T> {
  constructor() {
    this.nbVertices = 0;
    this.adjList = new Map<T, ExchangeRate[]>();
  }

  getAdjList(): Map<T, ExchangeRate[]> {
    return this.adjList;
  }

  addVertex(vertex: T): void {
    this.adjList.set(vertex, []);
    this.nbVertices += 1;
  }

  getNeighbors(vertex: T): ExchangeRate[] {
    const ret = this.adjList.get(vertex);

    if (!ret) {
      throw new Error('This vertex does not exist.');
    }
    return ret;
  }

  abstract addEdge(from: T, to: T, amount: number): void;

  abstract shortestPath(from: T, to: T): T[];

  protected nbVertices: number;
  protected adjList: Map<T, ExchangeRate[]>;
}
