import { CurrencyGraph } from './CurrencyGraph';
import { ExchangeRate } from './ExchangeRate';

export class CurrencyConverter {
  static compute(path: string[], amount: number, graph: CurrencyGraph): number {
    const adjList = graph.getAdjList();
    let result = amount;

    for (let i: number = 0; i < path.length - 1; i += 1) {
      const currency = adjList.get(path[i]);
      let rate: ExchangeRate | undefined;

      if (!currency) {
        throw new Error('Unexpected error.');
      }

      rate = currency.find(rate => rate.to === path[i + 1]);

      if (!rate) {
        throw new Error('Unexpected error.');
      }

      result *= parseFloat(rate.amount.toFixed(4));
    }
    return result;
  }
}
