export class ExchangeRate {
  constructor(readonly from: string, readonly to: string, readonly amount: number) { }
}
